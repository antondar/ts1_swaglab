# Semester work on the subject TS1(Software testing).

We are testing simple simulator of [internet store](https://www.saucedemo.com/) where you can choose different items. All items are stored in the cart, where you can see the basic information about us (name, surname and zip code).

# Authors 
- Daniil Lebedev(lebeddan)
- Daria Antonova(antondar)

# Used technologies 
- JUint 5
- Mockito
- Selenium
