package unitTests;

import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;

public class CartItems
{
    private WebDriver driver;
    List<String> exeptedItemsNames;

    @BeforeEach
    void setUp()
    {
        System.setProperty("webdriver.chrome.driver", "/Users/darya/Desktop/chromedriver");
        driver = new ChromeDriver();

        String expectedItem1 = "Sauce Labs Backpack";
        String expectedItem2 = "Sauce Labs Bike Light";
        exeptedItemsNames = new ArrayList();
    }

    void openPage() throws InterruptedException
    {
        driver.get("https://www.saucedemo.com");
        Thread.sleep(1000);
    }

    void login(String username, String password) throws InterruptedException
    {
        WebElement userNameField = driver.findElement(By.id("user-name"));
        WebElement passwordField = driver.findElement(By.id("password"));
        WebElement submitButton = driver.findElement(By.id("login-button"));

        userNameField.sendKeys(username);
        passwordField.sendKeys(password);
        submitButton.click();
    }

    @Test
    void addItemToCartTest() throws InterruptedException
    {
        String user = "standard_user";
        String password = "secret_sauce";

        String expectedItem1 = "Sauce Labs Backpack";
        String expectedItem2 = "Sauce Labs Bike Light";

        exeptedItemsNames.add(expectedItem1);
        exeptedItemsNames.add(expectedItem2);

        openPage();
        login(user, password);

        WebElement addItem1 = driver.findElement(By.id("add-to-cart-sauce-labs-backpack"));
        WebElement addItem2 = driver.findElement(By.id("add-to-cart-sauce-labs-bike-light"));
        WebElement cart = driver.findElement(By.id("shopping_cart_container"));

        addItem1.click();
        Thread.sleep(1000);
        addItem2.click();
        Thread.sleep(1000);
        cart.click();
        Thread.sleep(1000);

        List<WebElement> cartInventory = driver.findElements(By.className("inventory_item_name"));
        List<String> itemsNames = new ArrayList();
        for (WebElement item : cartInventory)
        {
            itemsNames.add(item.getText());
        }
        Assertions.assertLinesMatch(exeptedItemsNames, itemsNames);
    }

    @Test
    void removeItemsTest() throws InterruptedException
    {
        String user = "standard_user";
        String password = "secret_sauce";

        String expectedItem2 = "Sauce Labs Bike Light";

        exeptedItemsNames.add(expectedItem2);

        openPage();
        login(user, password);

        WebElement addItem1 = driver.findElement(By.id("add-to-cart-sauce-labs-backpack"));
        WebElement addItem2 = driver.findElement(By.id("add-to-cart-sauce-labs-bike-light"));
        WebElement cart = driver.findElement(By.id("shopping_cart_container"));

        addItem1.click();
        Thread.sleep(1000);
        addItem2.click();
        Thread.sleep(1000);
        cart.click();
        Thread.sleep(1000);

        WebElement removeItem = driver.findElement(By.id("remove-sauce-labs-backpack"));
        removeItem.click();
        List<WebElement> cartInventory = driver.findElements(By.className("inventory_item_name"));
        List<String> itemsNames = new ArrayList();
        for (WebElement item : cartInventory)
        {
            itemsNames.add(item.getText());
        }
        Assertions.assertLinesMatch(exeptedItemsNames, itemsNames);
    }

}
