import loging.LoginPage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UnitTests {
    private LoginPage loginPage = new LoginPage();
    private WebDriver driver = loginPage.getDriver();

    @Test
    public void loggingStandardUserTest() throws InterruptedException {
        loginPage.labsLoginStandard();
        Assertions.assertEquals("https://www.saucedemo.com/inventory.html",
                                loginPage.getDriver().getCurrentUrl());
    }

    @Test
    public void loggingProblemUserTest() throws InterruptedException {
        loginPage.loginProblemUser();
        Assertions.assertEquals("https://www.saucedemo.com/inventory.html",
                                loginPage.getDriver().getCurrentUrl());
    }

    @Test
    public void loggingLockedUserTest() throws InterruptedException {
        loginPage.loginLockedUser();
        Assertions.assertEquals("https://www.saucedemo.com/",
                                loginPage.getDriver().getCurrentUrl());
    }

    @Test
    public void loggingPerformanceUserTest() throws InterruptedException {
        loginPage.loginPerformanceUser();
        Assertions.assertEquals("https://www.saucedemo.com/inventory.html",
                                loginPage.getDriver().getCurrentUrl());
    }

    @Test
    public void setPriceLowToHighTest() throws InterruptedException {
        loginPage.labsLoginStandard();
        WebElement filter = driver.findElement(By.xpath("//*[@id=\"header_container\"]/div[2]/div[2]/span/select/option[3]"));
        filter.click();
        WebElement price1 = driver.findElement(By.xpath("//*[@id=\"inventory_container\"]/div/div[1]/div[2]/div[2]/div"));
        WebElement price2 = driver.findElement(By.xpath("//*[@id=\"inventory_container\"]/div/div[6]/div[2]/div[2]/div"));
        String p1 = price1.getText().substring(1);
        String p2 = price2.getText().substring(1);
        Assertions.assertTrue(Float.parseFloat(p1) < Float.parseFloat(p2));
    }

    @Test
    public void setPriceHighToLowTest() throws InterruptedException {
        loginPage.labsLoginStandard();
        WebElement filter = driver.findElement(By.xpath("//*[@id=\"header_container\"]/div[2]/div[2]/span/select/option[4]"));
        filter.click();
        WebElement price1 = driver.findElement(By.xpath("//*[@id=\"inventory_container\"]/div/div[1]/div[2]/div[2]/div"));
        WebElement price2 = driver.findElement(By.xpath("//*[@id=\"inventory_container\"]/div/div[6]/div[2]/div[2]/div"));
        String p1 = price1.getText().substring(1);
        String p2 = price2.getText().substring(1);
        Assertions.assertTrue(Float.parseFloat(p1) > Float.parseFloat(p2));
    }

    @Test
    public void errorInformationTest() throws InterruptedException {
        loginPage.labsLoginStandard();
        WebElement addBackpackButton = driver.findElement(By.id("add-to-cart-sauce-labs-backpack"));
        WebElement cartButton = driver.findElement(By.id("shopping_cart_container"));

        addBackpackButton.click();
        cartButton.click();
        Thread.sleep(1000);

        WebElement checkoutButton = driver.findElement(By.id("checkout"));
        checkoutButton.click();
        Thread.sleep(1000);

        WebElement continueButton = driver.findElement(By.id("continue"));
        continueButton.click();
        String errorMessage = driver.findElement(By.xpath("//*[@id=\"checkout_info_container\"]/div/form/div[1]/div[4]/h3")).getText();

        Assertions.assertEquals("Error: First Name is required", errorMessage);
    }


    @AfterEach
    public void clean() {
        loginPage.getDriver().quit();
    }

}
