package pages;

import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Login Test
 */
public class LoginPage {
    private WebDriver driver;

    public LoginPage() {
        init();
    }

    @BeforeEach
    public void init() {
        System.setProperty("webdriver.chrome.driver", "/Users/darya/Desktop/chromedriver");
        this.driver = new ChromeDriver();
    }

    public void openSwagLab() throws InterruptedException {
        driver.get("https://www.saucedemo.com");
        Thread.sleep(1000);
    }

    public void swagLabsLogin(String user, String password, WebDriver driver) throws InterruptedException {
        WebElement userNameField = driver.findElement(By.id("user-name"));
        WebElement passwordField = driver.findElement(By.id("password"));
        WebElement submitButton = driver.findElement(By.id("login-button"));

        userNameField.sendKeys(user);
        passwordField.sendKeys(password);
        submitButton.click();
    }

    public void labsLoginStandard() throws InterruptedException {
        openSwagLab();
        String user = "standard_user";
        String password = "secret_sauce";
        swagLabsLogin(user, password, this.driver);
    }

    public void loginLockedUser() throws InterruptedException {
        openSwagLab();
        String user = "locked_out_user";
        String password = "secret_sauce";
        swagLabsLogin(user, password, this.driver);
    }

    public void loginProblemUser() throws InterruptedException {
        openSwagLab();
        String user = "problem_user";
        String password = "secret_sauce";
        swagLabsLogin(user, password, this.driver);
    }

    public void loginPerformanceUser() throws InterruptedException {
        openSwagLab();
        String user = "performance_glitch_user";
        String password = "secret_sauce";
        swagLabsLogin(user, password, this.driver);
    }

    public void getUrl(){

    }

    public WebDriver getDriver() {
        return driver;
    }
}
